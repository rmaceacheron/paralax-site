<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Paralax Demo</title>
    <!--Mobile specific meta goodness :)-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/common.css" rel="stylesheet">
    
    <!-- Plugins -->

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/vendor/html5shiv.js"></script>
    <![endif]-->

  </head>
  <body>
    <div id="bodyBackgroundOne">
    </div>

    <div id="bodyBackgroundTwo">
    </div>

    <div id="bodyBackgroundThree">
    </div>

    <img id="logo"  />
    <div id="nav">
      <ul id="navList">
        <li class="navListItem"><a class="navListItemTag" href="#one">HOME</a></li>
        <li class="navListItem"><a class="navListItemTag" href="#four">ABOUT</a></li>
        <li class="navListItem"><a class="navListItemTag" href="#five">SERVICES</a></li>
        <li class="navListItem"><a class="navListItemTag" href="#six">GALLERY</a></li>
        <li class="navListItem"><a class="navListItemTag" href="#eight">VIDEOS</a></li>
      </ul>
    </div>

    <section id="one" data-type="sprite" data-offsetY="-100" data-Xposition="50%" data-speed="10">
      <h1 id="title">Skintreet Salon & Spa</h1>
    </section>

    <section id="two" data-speed="8" data-type="background">
      <div id="title" data-type="sprite" data-offsetY="100" data-Xposition="50%" data-speed="100">
      </div>
      <div id="one-content" class="row-fluid">
        <div class="span12">
          <h1 id="mainTitle">
          <span class="fontSize5" style="text-shadow: 1px 1px 1px #999;">Go ahead - <span class="specialTextGreen" style="text-shadow: 1px 1px 1px #576948;">treat yourself</span></span>
          <br/>
          <span class="fontSize4"><span class="">Look and</span> <span class="specialTextGreen" style="text-shadow: 1px 1px 1px #576948;">feel</span> <span class="">beautiful</span></span>
          </h1>
        </div>
      </div>
      
    </section>

    <section id="three" data-speed="8" data-type="background">

    </section>

    <section id="four" data-speed="8" data-type="background">
      <div id="one-content" class="row-fluid">
        <div class="span12">
          <h2 class="sectionTitle">ABOUT</h2>
        </div>
      </div>

      <div id="four-pic1" data-type="sprite" data-offsetY="300" data-Xposition="50%" data-speed="-10"></div>
      <div id="four-pic2" data-type="sprite" data-offsetY="300" data-Xposition="50%" data-speed="10"></div>
      <div id="four-pic3" data-type="sprite" data-offsetY="300" data-Xposition="50%" data-speed="-10"></div>
      <div id="four-pic4" data-type="sprite" data-offsetY="300" data-Xposition="50%" data-speed="-10"></div>
      <div id="four-pic5" data-type="sprite" data-offsetY="300" data-Xposition="50%" data-speed="10"></div>
      <div id="four-pic6" data-type="sprite" data-offsetY="300" data-Xposition="50%" data-speed="-10"></div>
    </section>

    <section id="five" data-speed="8" data-type="background">
      <div id="one-content" class="row-fluid">
        <div class="span12">
          <h2 class="sectionTitle">Services</h2>
        </div>
      </div>
    </section>

    <section id="six" data-speed="8" data-type="background">
      <div id="one-content" class="row-fluid">
        <div class="span12">
          <h2 class="sectionTitle">GALLERY</h2>
        </div>
      </div>

      <div id="six-pic1" data-type="sprite" data-offsetY="100" data-Xposition="0%" data-speed="20"></div>
      <div id="six-pic2" data-type="sprite" data-offsetY="100" data-Xposition="50%" data-speed="20"></div>
      <div id="six-pic3" data-type="sprite" data-offsetY="100" data-Xposition="50%" data-speed="20"></div>
      <div id="six-pic4" data-type="sprite" data-offsetY="100" data-Xposition="50%" data-speed="20"></div>
      <div id="six-pic5" data-type="sprite" data-offsetY="100" data-Xposition="50%" data-speed="20"></div>
      <div id="six-pic6" data-type="sprite" data-offsetY="100" data-Xposition="50%" data-speed="20"></div>
    </section>

    <section id="seven" data-speed="8" data-type="background">

    </section>

    <section id="eight" data-speed="8" data-type="background">
      <div id="one-content" class="row-fluid">
        <div class="span12">
          <h2 class="sectionTitle">VIDEO</h2>
        </div>
      </div>
    </section>


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery-1.10.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
    <script src="assets/js/scrollTo.min.js"></script>

    <script>
      $(document).ready(function() {
        $('.navListItemTag').scrollTo(); 
      });
    </script>
  </body>
</html>